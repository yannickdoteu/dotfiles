# Yannick's config files

Automatically install your config files on any system that has `curl`.

## Usage

```bash
curl -L config.yannick.eu | bash
```
(The `-L` option needs to be set for `curl` because it needs to follow at least one redirect)
Replace `bash` with `zsh` or the shell of your preference.


## Forking this project for own use
To use this project for own use, you'll have to:

1. Fork [this project](https://gitlab.com/yannickdoteu/config) in Gitlab
2. (Optionally) add your own domain via 'Deploy' -> 'Pages'
3. Change the Gitlab Pages URL in the `$GITLAB_PAGES_URL` variable in the `/public/index.html.sh` file
4. Add your own files as described below

You now have your own config always ready at hand when running this command on any machine with an internet connection.

## Note on .\*rc files

In [the installation script](public/index.html.sh) ~/.vimrc, ~/.zshrc and ~/.bashrc files are automatically symlinked to ~/.config/myvimrc, ~/.config/myzshrc, ~/.config/mybashrc, because all config files in this repository are put in ~/.config by default.
