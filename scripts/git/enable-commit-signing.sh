#!/bin/bash

source ~/.bashrc
set -e

KEY_ID=$(gpg-get-key-id)
if [ -z $KEY_ID ]; then
	gpg-generate-key
	echo
	echo "New GPG key generated. Here is the public block:"
	gpg-show-public-key
fi

git config --global user.signingKey $KEY_ID
git config --global tag.gpgSign true
git config --global commit.gpgSign true
