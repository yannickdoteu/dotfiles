#!/bin/bash

# Define the output file
OUTPUT_FILE="declarations.hpp"

# Define the include guard name
GUARD_NAME="DECLARATIONS_HPP"

# Create or clear the output file
cat <<EOF > "$OUTPUT_FILE"
#ifndef ${GUARD_NAME}
#define ${GUARD_NAME}

EOF

# Extract class names, remove any leading whitespace, format them as forward declarations, and append to the output file
grep -h -E 'class\s+[A-Za-z_][A-Za-z0-9_]*' *.hpp | sed -E 's/class\s+([A-Za-z_][A-Za-z0-9_]*).*/class \1;/' | sed 's/^[ \t]*//' | sort | uniq >> "$OUTPUT_FILE"

# Add the include guard end
cat <<EOF >> "$OUTPUT_FILE"

#endif // ${GUARD_NAME}
EOF

echo "Class declarations extracted to $OUTPUT_FILE"

