if [ -n "$GPG_EMAIL" ]; then
	gpg --quick-generate-key "$USER <$GPG_EMAIL>" rsa4096
else
	echo "Please set \$GPG_EMAIL first. (gpgkey.$USER.at.$HOSTNAME.$(date +'%d%m%y'))"
fi
