set -e

gpg --armor --export $(gpg-get-key-id)
