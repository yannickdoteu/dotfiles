-- Helper function to capitalize the first letter of a string
local function capitalize_first_letter(str)
    return str:gsub("^%l", string.upper)
end

-- Store original file contents for undo functionality
local original_files = {}

local function generate_class()
    -- Get the full path of the current header file being edited
    local header_file_path = vim.fn.expand('%:p')
    local header_dir = vim.fn.fnamemodify(header_file_path, ":h")  -- Directory of the header file
    local project_root = vim.fn.fnamemodify(header_dir, ":h")  -- Assume /include is inside the project root
    local filename = vim.fn.expand('%:t:r')  -- Extract the base filename without extension
    local class_name = capitalize_first_letter(filename)
    local guard = string.upper(filename) .. "_HPP"

    -- Initialize tables
    local includes = {}
    local attributes = {}
    local getters = {}
    local init_list = {}
    local assignment_list = {}
    local constructor_params = {}
    local constructor_initializers = {}  -- For initializer list in the constructor
    local constructor_assignments = {}
    local copy_constructor_init_list = {}  -- For initializer list in copy constructor, using getters
    local methods = {}

    -- Save original header file content for undo, if not already saved
    if not original_files[header_file_path] then
        original_files[header_file_path] = vim.fn.readfile(header_file_path)
    end

    -- Read the current buffer for attributes and includes
    local buf = vim.api.nvim_buf_get_lines(0, 0, -1, false)
    for _, line in ipairs(buf) do
        if line:match("^(%s*#include)") then
            -- Store #include lines
            table.insert(includes, line)
        else
            -- Extract attributes and generate getters, init list, and assignment
            local attr_type, attr_name = line:match("(%S+)%s+(%S+);")
            if attr_type and attr_name then
                table.insert(attributes, line)
                
                -- Mark getter functions as const
                table.insert(getters, string.format("%s get%s() const;", attr_type, capitalize_first_letter(attr_name)))
                table.insert(init_list, string.format("%s()", attr_name))
                table.insert(constructor_params, string.format("%s %s", attr_type, attr_name))
                table.insert(constructor_initializers, string.format("%s(%s)", attr_name, attr_name))  -- Add to initializer list
                table.insert(constructor_assignments, string.format("this->%s = %s;", attr_name, attr_name))
                
                -- Modify copy constructor to use the getter method
                table.insert(copy_constructor_init_list, string.format("%s(copy.get%s())", attr_name, capitalize_first_letter(attr_name)))
                
                -- Modify assignment operator to use getter methods for rhs
                table.insert(assignment_list, string.format("this->%s = rhs.get%s();", attr_name, capitalize_first_letter(attr_name)))

                table.insert(methods, string.format([[
%s %s::get%s() const {
    return (this->%s);
};
]], attr_type, class_name, capitalize_first_letter(attr_name), attr_name))
            end
        end
    end

    -- Read template files
    local header_template_path = vim.fn.expand("~/.config/nvim/templates/class_header.hpp")
    local source_template_path = vim.fn.expand("~/.config/nvim/templates/class_source.cpp")
    local header_template = table.concat(vim.fn.readfile(header_template_path), "\n")
    local source_template = table.concat(vim.fn.readfile(source_template_path), "\n")

    -- Replace placeholders with actual content
    header_template = header_template:gsub("${GUARD}", guard)
                                     :gsub("${INCLUDES}", table.concat(includes, "\n"))
                                     :gsub("${CLASS_NAME}", class_name)
                                     :gsub("${GETTERS}", table.concat(getters, "\n\t\t"))
                                     :gsub("${ATTRIBUTES}", table.concat(attributes, "\n\t\t"))
                                     :gsub("${CONSTRUCTOR_PARAMS}", table.concat(constructor_params, ", "))
    
    source_template = source_template:gsub("${CLASS_NAME}", class_name)
                                     :gsub("${INIT_LIST}", table.concat(init_list, ", "))
                                     :gsub("${ASSIGNMENT_LIST}", table.concat(assignment_list, "\n\t"))
                                     :gsub("${CONSTRUCTOR_INITIALIZERS}", table.concat(constructor_initializers, ", "))
                                     :gsub("${CONSTRUCTOR_PARAMS}", table.concat(constructor_params, ", "))
                                     :gsub("${COPY_CONSTRUCTOR_INIT_LIST}", table.concat(copy_constructor_init_list, ", "))
                                     :gsub("${METHODS}", table.concat(methods, "\n"))

    -- Write the header file (overwrite the current one)
    vim.fn.writefile(vim.split(header_template, "\n"), header_file_path)

    -- Determine the source file path
    local src_dir = header_dir -- Default: source file is in the same directory as the header
    if string.match(header_dir, "/include$") then
        -- If the header file is inside an /include directory, put the source file in /src
        src_dir = project_root .. "/src"
        vim.fn.mkdir(src_dir, "p")
    end

    -- Write the source file in the appropriate location
    local source_file = src_dir .. "/" .. filename .. ".cpp"
    vim.fn.writefile(vim.split(source_template, "\n"), source_file)

    -- Print message
    print("Generated " .. header_file_path .. " and " .. source_file)

    -- Automatically reload the current buffer to reflect changes
    vim.cmd("e!")

    -- Split open the source file
    vim.cmd("FSSplitLeft")
end

local function generate_class_undo()
    -- Get the full path of the current header file being edited
    local header_file_path = vim.fn.expand('%:p')

    -- Restore the original content if available
    if original_files[header_file_path] then
        vim.fn.writefile(original_files[header_file_path], header_file_path)
        -- Clear the stored content after restoring
        original_files[header_file_path] = nil
        print("Restored original content of " .. header_file_path)
        -- Reload the buffer to reflect the restored content
        vim.cmd("e!")
    else
        print("No original content found to restore.")
    end
end

-- Bind the functions to commands
vim.api.nvim_create_user_command('GenerateClass', generate_class, {})
vim.api.nvim_create_user_command('GenerateClassUndo', generate_class_undo, {})
