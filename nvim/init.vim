" NOTES
" vim for c++ : https://idie.ru/posts/vim-modern-cpp

let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" plugins installed by vim-plug
call plug#begin()
Plug 'https://github.com/derekwyatt/vim-fswitch.git'
Plug 'https://github.com/ludovicchabant/vim-gutentags.git'
call plug#end()
autocmd VimEnter *
  \  if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \|   PlugInstall --sync | q
  \| endif

"enable :Man plugin and open man page vertically
runtime ftplugin/man.vim
let g:ft_man_open_mode = 'vert'

" misc settings
set number
set tabstop=4
set	shiftwidth=4
syntax on
set autoindent
set noexpandtab
let g:user42 = 'yaretel-'
let g:mail42 = 'yaretel-@student.s19.be'
set hlsearch

" ctags optimization
set autochdir
set tags=tags;

" fold
set foldmethod=indent
set foldlevelstart=10

" auto highlight trailing whitespaces
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
au BufWinEnter * match ExtraWhitespace /\s\+$/
au InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
au InsertLeave * match ExtraWhitespace /\s\+$/
au BufWinLeave * call clearmatches()

" Remove all trailing whitespaces
nnoremap <silent> <leader>rs :let _s=@/ <Bar> :%s/\s\+$//e <Bar> :let @/=_s <Bar> :nohl <Bar> :unlet _s <CR>

" Remove :: as seperator when using K for manual lookup
function! s:JbzCppMan()
    let old_isk = &iskeyword
    setl iskeyword+=:
    let str = expand("<cword>")
    let &l:iskeyword = old_isk
    execute '!cppman ' . str
endfunction
command! JbzCppMan :call s:JbzCppMan()
au FileType cpp nnoremap <buffer>K :JbzCppMan<CR>

" switching from and to header files
au BufEnter *.h  let b:fswitchdst = 'c,cpp,cc,m' | let b:fswitchlocs = 'reg:|include.*|src/**|'
au BufEnter *.h  let b:fswitchdst = 'h,hpp'

nnoremap <silent> <localleader>or :FSHere<cr>
" Extra hotkeys to open header/source in the split
nnoremap <silent> <localleader>oh :FSSplitLeft<cr>
nnoremap <silent> <localleader>oj :FSSplitBelow<cr>
nnoremap <silent> <localleader>ok :FSSplitAbove<cr>
nnoremap <silent> <localleader>ol :FSSplitRight<cr>

" config for gutentags
set tags=./tags;
let g:gutentags_ctags_exclude_wildignore = 1
let g:gutentags_ctags_exclude = [
  \'node_modules', '_build', 'build', 'CMakeFiles', '.mypy_cache', 'venv',
  \'*.md', '*.tex', '*.css', '*.html', '*.json', '*.xml', '*.xmls', '*.ui']

luafile ~/.config/nvim/lua/class_generator.lua
luafile ~/.config/nvim/lua/custom_filetypes.lua
set mouse=
