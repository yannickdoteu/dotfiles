#ifndef ${GUARD}
#define ${GUARD}

${INCLUDES}

class ${CLASS_NAME} {
    public:
        ${CLASS_NAME}();
        ${CLASS_NAME}(const ${CLASS_NAME}& copy);
        ${CLASS_NAME}( ${CONSTRUCTOR_PARAMS} );
        ~${CLASS_NAME}();
        ${CLASS_NAME}& operator=(const ${CLASS_NAME}& rhs);

        ${GETTERS}

	private:
        ${ATTRIBUTES}
};

#endif
