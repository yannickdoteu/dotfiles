#include "${CLASS_NAME}.hpp"

${CLASS_NAME}::${CLASS_NAME}() : ${INIT_LIST} {}

${CLASS_NAME}::${CLASS_NAME}( ${CONSTRUCTOR_PARAMS} )
    : ${CONSTRUCTOR_INITIALIZERS} {}

${CLASS_NAME}::${CLASS_NAME}(const ${CLASS_NAME}& copy)
    : ${COPY_CONSTRUCTOR_INIT_LIST} {}

${CLASS_NAME}::~${CLASS_NAME}() {}

${CLASS_NAME}& ${CLASS_NAME}::operator=(const ${CLASS_NAME}& rhs) {
    ${ASSIGNMENT_LIST}
    return *this;
}

${METHODS}
