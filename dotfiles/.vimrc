if empty($XDG_CONFIG_HOME)
  let $XDG_CONFIG_HOME = expand('~/.config')
endif

source $XDG_CONFIG_HOME/nvim/init.vim
