if [ $(uname) = 'Darwin' ]; then
	alias 42-install-brew='rm -rf $HOME/goinfre/.brew && git clone --depth=1 https://github.com/Homebrew/brew $HOME/goinfre/.brew && export PATH=$HOME/goinfre/.brew/bin:$PATH && brew update'
	alias ctags="`brew --prefix`/bin/ctags"
	# For installing cppman
	alias 42-install-cppman="brew install cppman && cppman -c && cppman -m true"
fi

# For my docker things
if [ $(uname) = 'Darwin' ]; then
	mkdir -p ~/goinfre/docker
	rm -rf ~/Library/Containers/com.docker.docker
	ln -s ~/goinfre/docker ~/Library/Containers/com.docker.docker
fi

# ALIAS
alias gl='git log --oneline --graph'
alias c="gcc -Werror -Wextra -Wall"

# DEVENV
if [ -z "$DEVENV" ]; then
	alias devenv='docker volume create devenv && docker run -it --rm --init -v "devenv:/root" registry.gitlab.com/yannickdoteu/devenv sh -c "cd /root; bash"'
	alias devenv-update='docker pull registry.gitlab.com/yannickdoteu/devenv:latest'
else
	export PS1="$DEVENV:\w\$ "
fi

# CONFIG
alias config-update='curl -L config.yannick.eu | bash; source ~/.bashrc'

## GPG (someday)
#export GPG_TTY=$(tty)
#gpgconf --launch gpg-agent
#alias gpg-generate-key='bash ~/.config/scripts/gpg/generate-key.sh'
#alias gpg-get-key-id='bash ~/.config/scripts/gpg/extract-first-key-id.sh'
#alias gpg-show-public-key='bash ~/.config/scripts/gpg/show-public-key-block.sh'

# Git
git config --global user.email "<>"
git config --global user.name "Yannick"
git config --global pull.rebase true
git config --global init.defaultBranch "main"
git config --global pull.rebase true
alias git-ignore-untracked='echo "$(git status --porcelain | grep '^??' | cut -c4-)" >> .gitignore'
#alias git-enable-commit-signing='bash ~/.config/scripts/git/enable-commit-signing.sh'

# C++
alias generate-declarations.hpp='bash ~/.config/scripts/c++/generate_declarations.hpp.sh'

# Vim
alias nvim-config='nvim ~/.config/nvim/init.vim'
if nvim -v >/dev/null; then
	alias vim=nvim
fi

# To verify that my bashrc got loaded
echo oeliwoelie

export PATH="$PATH:/opt/nvim-linux64/bin"
alias vim=nvim
