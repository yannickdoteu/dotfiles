#!/bin/bash

# ============================================================ #
#                                                              #
#                  YANNICK'S CONFIG FILES                      #
#                                                              #
#   More information: https://gitlab.com/yannickdoteu/config   #
#                                                              #
# ============================================================ #

set +x

# check if run in bash
if [ -z "$BASH_VERSION" ]; then
	echo 'Not running in Bash. Try again in Bash.'
	exit 1
fi

check_deps () {
	for i in $@
	do
		if ! type $i > /dev/null 2>&1; then
			export MISSING_DEPS+=" '$i'"
		fi
	done
}

set_download_command () {
	# check dependencies and set tools
	if ! type curl > /dev/null; then
		DOWNLOAD_COMMAND='wget'
	else
		DOWNLOAD_COMMAND='curl -O'
	fi
}

exit_if_missing_deps () {
	if [ -n "$MISSING_DEPS" ]; then
		echo "$MISSING_DEPS not found. Please install them and try again."
		exit 1
	fi
}

install_by_tar () {

	# clean up
	rm -f "$GL_REPO-$MAIN_BRANCH.tar.gz"
	rm -rf "$GL_REPO-$MAIN_BRANCH"

	set_download_command
	# download tar
	$DOWNLOAD_COMMAND https://$GL_INSTANCE/$GL_USERNAME/$GL_REPO/-/archive/$MAIN_BRANCH/$GL_REPO-$MAIN_BRANCH.tar.gz
	if [ $? -ne 0 ]; then
		echo "$DOWNLOAD_COMMAND failed..."
		exit 1
	fi

	# extract and move to config
	tar -xzf $GL_REPO-$MAIN_BRANCH.tar.gz
	cp -R $GL_REPO-$MAIN_BRANCH/* $HOME/.config

	# clean up
	rm -f "$GL_REPO-$MAIN_BRANCH.tar.gz"
	rm -rf "$GL_REPO-$MAIN_BRANCH"
}

check_clone_permission () {
	git ls-remote git@$GL_INSTANCE:$GL_USERNAME/$GL_REPO.git > /dev/null
}

install_by_git () {

	# clean up
	rm -rf $HOME/.config/.git $HOME/.config/.config.tmp

	git clone --no-checkout git@gitlab.com:yannickdoteu/config.git $HOME/.config/.config.tmp
	if [ $? -ne 0 ]; then
		echo "git clone failed even though there is permission to clone? idk what happened..."
		exit 1
	fi
	mv $HOME/.config/.config.tmp/.git $HOME/.config
	rmdir $HOME/.config/.config.tmp
	cd $HOME/.config
	git reset --hard HEAD
	cd -
}

set_env () {
	# set env vars if needed
	if [ -z "$HOME" ]; then
		export HOME=~
	fi
	if [ -z "$XDG_CONFIG_HOME" ]; then
		export XDG_CONFIG_HOME=$HOME/.config
	fi
}

link_dotfiles () {
	# remove and link dotfiles
	rm $HOME/.bashrc $HOME/.zshrc $HOME/.vimrc

	ln -s $HOME/.config/dotfiles/.bashrc $HOME/.bashrc
	echo 'dont forget to run `source ~/.bashrc` if you ran this in new shell'

	ln -s $HOME/.config/dotfiles/.zshrc $HOME/.zshrc

	ln -s $HOME/.config/dotfiles/.vimrc $HOME/.vimrc
}

# script parameters
GL_INSTANCE=gitlab.com
GL_USERNAME=yannickdoteu
GL_REPO=config
MAIN_BRANCH=main
set_env

MISSING_DEPS=""
check_deps git
if [ -z "$MISSING_DEPS" ]; then
	check_clone_permission
	if [ $? -eq 0 ]; then
		install_by_git
		link_dotfiles
		exit 0
	else
		echo -n "No permission to clone via git... Make sure you have write permission to $GL_INSTANCE/$GL_USERNAME/$GL_REPO over ssh. "
	fi
else
	echo -n "Missing $MISSING_DEPS. Install them and try again if you want write access to the remote. "
fi
echo "Will install as read-only."
echo

MISSING_DEPS=""
check_deps tar curl
if [ -z "$MISSING_DEPS" ]; then
	install_by_tar
	link_dotfiles
	exit 0
else
	echo -n "Missing $MISSING_DEPS. "
fi
echo "Will try another method."

MISSING_DEPS=""
check_deps tar wget
if [ -z "$MISSING_DEPS" ]; then
	install_by_tar
	link_dotfiles
	exit 0
else
	echo -n "Missing $MISSING_DEPS. "
fi

echo " Please install missing dependencies and try again.."
